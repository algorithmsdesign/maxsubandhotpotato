/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.maxsubarrayandhotpotato;

/**
 *
 * @author Pattrapon N
 */
public class MaxSubArray {
     public static int max(int a, int b) {
        if (a > b) {
            return a;
        } else if (b > a) {
            return b;
        }
        return a;
    }
    // 3
    public static int maxSubFastest(int[] A) {
        int[] M = new int[A.length];
        M[0] = 0;
        int m;
        for (int t = 1; t < A.length; t++) {
            System.out.println(A[t]);
            M[t] = max(0, M[t - 1] + A[t]);
        }
        m = 0;
        for (int t = 1; t < A.length; t++) {
            m = Math.max(m, M[t]);

        }
        return m;
    }

    public static void main(String[] args) {
        int[] A = {0, 3, -9, -4, -7};
        int m = maxSubFastest(A);

        System.out.println(m);

    }

}
